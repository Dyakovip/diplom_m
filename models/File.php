<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $name_physical
 * @property string $name_logical
 * @property string $date_upload
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_physical', 'name_logical'], 'required'],
            [['date_upload'], 'safe'],
            [['name_physical', 'name_logical'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_physical' => 'Name Physical',
            'name_logical' => 'Name Logical',
            'date_upload' => 'Date Upload',
        ];
    }


}
