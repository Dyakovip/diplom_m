<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "practice".
 *
 * @property int $id
 * @property int $type Наименование практики
 * @property int $course Уровень образования
 * @property int $level Курс
 * @property int $id_file Файл
 * @property int $id_user Пользователь
 *
 * @property File $file
 * @property User $user
 */
class Practice extends \yii\db\ActiveRecord
{

    const TYPE_PRACTICE_EDUCATIONAL = 1;
    const TYPE_PRACTICE_PRODUCTION = 2;
    const TYPE_PRACTICE_UNDERGRADUATE = 3;


    public $file_report;
    public $file_review;
    public $file_progress;
    public $file_method;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'practice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'course', 'level', 'id_file', 'id_user'], 'required'],
            ['user_group', 'string', 'max' => 500],
            [['type', 'course', 'level', 'id_file', 'id_user'], 'integer'],
            [['id_file'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['id_file' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип практики',
            'course' => 'Курс',
            'level' => 'Уровень образования',
            'id_file' => 'Файл',
            'id_user' => 'Пользователь',
            'user_group' => 'Группа'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'id_file']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
