<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\User;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="header">
    <div class="logo">
        <?php echo Html::a(Yii::$app->name, Yii::$app->getHomeUrl()); ?>
    </div>
</div>


<div class="mainmenu">
    <?php
    NavBar::begin([
        'options' => [
            'class' => 'mainmenu',
        ],
    ]);

    $exitLabel = User::getFullName();

    $items = [];
    if (Yii::$app->user->isGuest) {
        $items = [['label' => 'Вход', 'url' => ['/site/login']]];
    } else if (User::isTeacher()) {
        $items = [
            ['label' => 'Бакалавриат', 'items' => [
                ['label' => '1 курс', 'url' => ['/site/index']],
                ['label' => '2 курс', 'url' => ['/site/index']],
                ['label' => '3 курс', 'url' => ['/site/index']],
                ['label' => '4 курс', 'url' => ['/site/index']],
                ['label' => '5 курс', 'url' => ['/site/index']],
                ['label' => '6 курс', 'url' => ['/site/index']],
            ]],
            ['label' => 'Магистратура', 'items' => [
                ['label' => '1 курс', 'url' => ['/site/index']],
                ['label' => '2 курс', 'url' => ['/site/index']],
                ['label' => '3 курс', 'url' => ['/site/index']],
            ]],
            ['label' => 'Аспирантура', 'items' => [
                ['label' => '1 курс', 'url' => ['/site/index']],
                ['label' => '2 курс', 'url' => ['/site/index']],
                ['label' => '3 курс', 'url' => ['/site/index']],
                ['label' => '4 курс', 'url' => ['/site/index']],
                ['label' => '5 курс', 'url' => ['/site/index']],
                ['label' => '6 курс', 'url' => ['/site/index']],
            ]],

            Yii::$app->user->isGuest ? (
            ['label' => 'Вход', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . $exitLabel . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ];
    } else {
        $items = [
            Yii::$app->user->isGuest ? (
            ['label' => 'Вход', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . $exitLabel . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $items
    ]);
    NavBar::end();
    ?>
</div>

<div class="basicbg">
    <div class="basic">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<!--</div>-->

<footer class="footer">
    <div class="container">
        <p class="pull-left">ПГГПУ <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
