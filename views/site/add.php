<?php

/* @var $this yii\web\View */

$this->title = 'Добавить практику';

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<style type="text/css">
    .cnt {
        margin: 10px;
        padding: 20px;
        background-color: snow;
    }

    .high {
        padding-bottom: 10px;
    }

    .load {

    }
</style>


<div class="cnt">
    <div class="row">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <div class="col-sm-3">
            <?= $form->field($model, 'type')->dropDownList([
                '1' => 'Учебная',
                '2' => 'Производственная',
                '3' => 'Преддипломная'
            ]);
            ?>
        </div>


        <div class="col-sm-3">
            <?= $form->field($model, 'level')->dropDownList([
                '1' => 'Бакалавриат',
                '2' => 'Магистратура',
                '3' => 'Аспирантура'
            ]);
            ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'course')->dropDownList([
                '1' => '1 курс',
                '2' => '2 курс',
                '3' => '3 курс',
                '4' => '4 курс',
                '5' => '5 курс',
                '6' => '6 курс'
            ]);
            ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'user_group')->dropDownList([
                '1212' => '1212',
                '1223' => '1223',
                '1235' => '1335',
                '1236' => '1336',
                '1237' => '1337',
                '1238' => '1338'
            ]);
            ?>

        </div>

    </div>

    <div class="load">
        <div class="row high">
            <div class="col-sm-2">
                <span class="label label-default"> Дневник-отчет </span>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'file_report')->fileInput(['class' => 'btn btn-success'])->label(false) ?>
            </div>
        </div>


        <div class="row high">
            <div class="col-sm-2">
                <span class="label label-default"> Отзыв руководителя </span>
            </div>
            <div class="col-sm-2">
                <?= Html::a('Прикрепить', ['add'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>


        <div class="row high">
            <div class="col-sm-2">
                <span class="label label-default"> Методические разработки </span>
            </div>
            <div class="col-sm-2">
                <?= Html::a('Прикрепить', ['add'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>


        <div class="row high">
            <div class="col-sm-2">
                <span class="label label-default">Достижения</span>
            </div>
            <div class="col-sm-2">
                <?= Html::a('Прикрепить', ['add'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php
    $form::end();
    ?>
</div>
